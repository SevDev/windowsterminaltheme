# wow this some super Windows Terminal Themes ༼ つ ◕_◕ ༽つ

Here are some noice Windows Terminal schemes and profiles.
Pictures are in the `res` folder.
Enjoy !

---
links:
- [guidgen.com](https://www.guidgen.com/) -> to create new guid
- [r/Portal post](https://www.reddit.com/r/Portal/comments/gsjfo9/windows_terminal_allows_customization_details_in/) -> Portal styled CMD
- [github/DankNeon](http://github.com/DankNeon) -> Dank Neon (⌐■_■)
- [github/mut-ex/wallpapers](https://github.com/mut-ex/wallpapers) -> Amazing wallpapers

---

<details>
<summary>schemes</summary>

### Dank Neon
```json
{
    "name": "Dank Neon",
    "background": "#191b2a",
    "foreground": "#eff0f6",

    "cursorColor": "#eff0f6",

    "black": "#191b2a",
    "brightBlack": "#2a2d46",

    "blue": "#7496cc",
    "brightBlue": "#94bfff",

    "cyan": "#01a8ad",
    "brightCyan": "#01f7f7",

    "green": "#2dad86",
    "brightGreen": "#39ffba",

    "purple": "#a37ca9",
    "brightPurple": "#f1b3f1",

    "red": "#ac3756",
    "brightRed": "#ff476e",

    "white": "#eff0f6",
    "brightWhite": "#eff0f6",

    "yellow": "#ffca7a",
    "brightYellow": "#ffee7a"
}
```
---
### Aperture Laboratories
```json
{
    "name": "Aperture Laboratories Scheme",
    "background": "#000",
    "foreground": "#A66900",

    "cursorColor": "#A66900",

    "black": "#A66900",
    "brightBlack": "#A66900",

    "blue": "#A66900",
    "brightBlue": "#A66900",

    "cyan": "#A66900",
    "brightCyan": "#A66900",

    "green": "#A66900",
    "brightGreen": "#A66900",

    "purple": "#A66900",
    "brightPurple": "#A66900",

    "red": "#A66900",
    "brightRed": "#A66900",

    "white": "#A66900",
    "brightWhite": "#A66900",

    "yellow": "#A66900",
    "brightYellow": "#A66900"
}
```
</details>

---

<details>
<summary>profiles</summary>

### Sev's Terminal
```json
{
    "guid": "{77777777-7777-7777-7777-777777777777}",
    "name": "Sev's terminal",
    "tabTitle": "YeetShell v7",

    "commandline": "powershell.exe -NoLogo -NoExit",
    "startingDirectory": "%USERPROFILE%",

    "colorScheme": "Dank Neon",
    "tabColor": "#00bcd4",

    "backgroundImage": "desktopWallpaper",
    "backgroundImageOpacity": 0.7,

    "icon": "🤔"
}
```
---
### GlassShell
```json
{
    "guid": "{d6bb6d3f-c16b-44e5-965d-81002ad609b1}",
    "name": "GlassShell",

    "commandline": "powershell.exe -NoLogo -NoExit",
    "startingDirectory": "%USERPROFILE%",

    "colorScheme": "Dank Neon",

    "useAcrylic": true,
    "acrylicOpacity": 0
}
```
---
### Dank Neon PowerShell
```json
{
    "guid": "{04541582-aced-4d7d-9d80-959f22125fe6}",
    "name": "Dank Neon PS",
    "tabTitle": "PowerShell",
    "hidden": false,

    "commandline": "powershell.exe -NoLogo -NoExit -Command echo 'Windows PowerShell' '(⌐■_■)' ''",
    "startingDirectory": "%USERPROFILE%",

    "colorScheme": "Dank Neon",
    "tabColor": "#191b2a",

    "backgroundImage": "<path-to-picture>/DN-ic.png",
    "backgroundImageAlignment": "bottomRight",
    "backgroundImageStretchMode": "none",
    "backgroundImageOpacity": 0.5,

    "icon": "<path-to-picture>/DN-ic.png"
}
```
---
### Aperture Laboratories PowerShell
```json
{
    "guid": "{092935ef-94ca-434a-8f4d-0f6bf8977b08}",
    "name": "Aperture Laboratories PS",
    "tabTitle": "PowerShell",
    "hidden": false,

    "commandline": "powershell.exe -NoLogo -NoExit -Command echo 'APERTURE LABORATORIES GLaDOS v7.14' 'COPYRIGHT (c) 1973-1997 APERTURE - ALL RIGHTS RESERVED' ''",
    "startingDirectory": "%USERPROFILE",

    "colorScheme": "Aperture Laboratories Scheme",
    "tabColor": "#A66900",
    "cursorShape": "vintage",

    "experimental.retroTerminalEffect": true,

    "backgroundImage": "<path-to-picture>/AL-bg.png",
    "backgroundImageAlignment": "bottomRight",
    "backgroundImageStretchMode": "none",
    "backgroundImageOpacity": 0.5,

    "icon": "<path-to-picture>/AL-ic.png"
}
```
<!--
Dank Neon Command Prompt
```json
{
    "guid": "{b9cbe74b-c64f-46bf-bd2c-18093e43e43e}",
    "name": "Dank Neon Cmd",
    "tabTitle": "Dank Neon Cmd",
    "hidden": false,

    "commandline": "cmd.exe /k cls && echo (⌐■_■)",
    "startingDirectory": "%USERPROFILE%",

    "colorScheme": "Dank Neon",
    "tabColor": "#191b2a",

    "backgroundImage": "<path-to-picture>/DN-ic.png",
    "backgroundImageAlignment": "bottomRight",
    "backgroundImageStretchMode": "none",
    "backgroundImageOpacity": 0.5,

    "icon": "<path-to-picture>/DN-ic.png"
}
```
-->
<!--
Aperture Laboratories Command Prompt
```json
{
    "guid": "{5d1d776f-eca2-43a1-88fd-a3c58c504c0d}",
    "name": "Aperture Laboratories Cmd",
    "tabTitle": "Aperture Laboratories Cmd",
    "hidden": false,

    "commandline": "cmd.exe /k cls && echo APERTURE LABORATORIES GLaDOS v7.14 && echo COPYRIGHT (c) 1973-1997 APERTURE - ALL RIGHTS RESERVED",
    "startingDirectory": "%USERPROFILE%",

    "colorScheme": "Aperture Laboratories Scheme",
    "tabColor": "#A66900",
    "cursorShape": "vintage",

    "experimental.retroTerminalEffect": true,

    "backgroundImage": "<path-to-picture>/AL-bg.png",
    "backgroundImageAlignment": "bottomRight",
    "backgroundImageStretchMode": "none",
    "backgroundImageOpacity": 0.5,
    
    "icon": "<path-to-picture>/AL-ic.png"
}
```
-->
</details>

---

<details>
<summary>previews</summary>

### Sev's Terminal
![Sev's Terminal profile preview](res/ST-preview.png)
---
### GlassShell
![GlassShell profile preview](res/GS-preview.png)
---
### Aperture Laboratories
![Aperture Laboratories profile preview](res/AL-preview.png)
---
### Dank Neon
![Dank Neon profile preview](res/DN-preview.png)
</details>

---

(⌐■_■)

---